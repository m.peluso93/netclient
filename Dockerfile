FROM balenalib/raspberrypi3-ubuntu-python

RUN apt-get update \
    && apt-get install -y tcpdump wireless-tools wpasupplicant dnsmasq \
    && rm -rf /var/cache/apk/* \
    && rm -rf /var/lib/apt/*
        

WORKDIR /opt/netclient
RUN mkdir -p /opt/netclient

COPY requirements.txt /opt/netclient/requirements.txt
RUN pip install -r requirements.txt

COPY networkclient.py /opt/netclient/networkclient.py

COPY ./pyshare/pyshare.py /opt/netclient/pyshare.py
ADD  ./sniffer/sniffer.py /opt/netclient/sniffer.py

CMD python networkclient.py
